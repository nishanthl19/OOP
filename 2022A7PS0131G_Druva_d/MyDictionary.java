// Make a dictionary class

import java.util.Arrays;

class Record {
    private double value;
    Record(double newValue) {
        this.value=newValue;
    }
    public double getValue() { return this.value; }
    public int getKey() {
        // key is closest integer to value
        return (int) Math.round(this.value);
    }
}
class MyDictionary {
    private Record[] records;
    private int[] keys;
    private int size=0;
    private int length = 0;
    public enum Ordering { NONE, ASC, DESC, PRIQASC, PRIQDESC, BST };
    private Ordering ordering = Ordering.NONE;
    public int Size() { return size; }
    public int Length() { return length; }
    public Record[] getRecords() { return records; }
    MyDictionary(int initialSize) {
        records=new Record[initialSize];
        keys=new int[initialSize];
        size=initialSize;
        length=0;
    }

    public boolean isFull() {
        return length==size;
    }

    public boolean isEmpty() {
        return length==0;
    }

    public Double get(int key) {
        System.out.println("key: " + key);
        int index = key_contains(key);
        if (index != -1) {
            // Replace the record
            System.out.println("index: " + index);
            return records[index].getValue();
        }
        else {
            return null;
        }
    }

    public Record remove(int key){
        int index = key_contains(key);
        if (index != -1) {
            // Replace the record
            Record oldRecord = records[index];
            for(int i = index; i < this.length - 1; i++){
                this.records[i] = this.records[i + 1];
                this.keys[i] = this.keys[i + 1];
            }
            this.length--;
            return oldRecord;
        }
        else {
            return null;
        }
    }

    private int key_contains(int key){
        for(int i = 0; i < this.length; i++){
            if(this.keys[i] == key){
                return i;
            }
        }
        return -1;

    }

    public Record put(Record record) {
        if(isFull()) {
            System.out.println("Dictionary is full");
            return null;
        }
        int index = key_contains(record.getKey());
        if (index != -1) {
            // Replace the record
            Record oldRecord = records[index];
            records[index] = record;
            return oldRecord;
        }
        else {
            // Add the record
            this.records[this.length] = record;
            this.keys[length] = record.getKey();
            this.length++;
            return null;
        }
    }

    public void records(){
        System.out.println("Printing Dictionary of " + this.size + " Size and " + this.length + " Occupancy:");
        for (int i = 0; i < this.length; i++) {
            System.out.println("value: " + this.records[i].getValue());
            System.out.println("key: " + this.records[i].getKey());
        }
        for (int i = this.length; i < this.size; i++) {
            System.out.println(" .");
        }
        System.out.println("");
    }

    public void keys(){
        System.out.println("Printing Dictionary of " + this.size + " Size and " + this.length + " Occupancy:");
        for (int i = 0; i < this.length; i++) {
            System.out.println("key: " + this.keys[i]);
        }
        for (int i = this.length; i < this.size; i++) {
            System.out.println(" .");
        }
        System.out.println("");
    }



}